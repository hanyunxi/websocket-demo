package com.hy.websocketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@EnableScheduling
public class ServiceTask {
    @Autowired
    private WebSocketToAll webSocketServer;
    @Autowired
    private WebSocketOneToOne oneToOne;
    /**
     * 服务端  向客户端  推送 消息
     */
    @Scheduled(initialDelay = 10000,fixedDelay = 1000 * 10)
    public void send(){
        int index = (int)(Math.random() * 5 + 1);
        List<String> language = Arrays.asList("Java", "Python", "Go", "C++", "Kotlin","Node");
        //webSocketServer.onMessage("Hello, "+language.get(index)+" !!!");
        oneToOne.sendInfo("30","Hello, "+language.get(index)+" !!!");
    }

}
